<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMasterData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mUserPrivilege', function (Blueprint $table) {
            $table->increments('PrivilegeId');
            $table->unsignedInteger('UserRoleId')->references('UserRoleId')->on('mUserRole');
            $table->string('ModuleName',50);
            $table->boolean('Open');
            $table->boolean('Edit');
            $table->boolean('Delete');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });

        Schema::create('mUserRole', function (Blueprint $table) {
            $table->increments('UserRoleId');
            $table->string('UserRoleName',50);
            $table->string('UserRoleDesc',50);
            $table->boolean('IsActive');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });

        Schema::create('mUser', function (Blueprint $table) {
            $table->increments('UserId');
            $table->unsignedInteger('UserRoleId')->references('UserRoleId')->on('mUserRole');;
            $table->string('UserName',50);
            $table->string('Password',16);
            $table->boolean('IsActive');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mUser');
        Schema::dropIfExists('mUserRole');
        Schema::dropIfExists('mUserPrivilege');
    }
}
