<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseMasterData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mWarehouse', function (Blueprint $table) {
            $table->increments('WarehouseId');
            $table->string('WarehouseName',50);
            $table->string('WarehouseAddress',100);
            $table->string('WarehousePhone1',20);
            $table->string('WarehousePhone2',20);
            $table->string('WarehousePIC',50);
            $table->boolean('IsActive');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });

        Schema::create('mOutlet', function (Blueprint $table) {
            $table->increments('OutletId');
            $table->string('OutletName',50);
            $table->string('OutletAddress',100);
            $table->string('OutletPhone1',20);
            $table->string('OutletPhone2',20);
            $table->string('OutletPIC',50);
            $table->boolean('IsActive');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mWarehouse');
        Schema::dropIfExists('mOutlet');
    }
}
