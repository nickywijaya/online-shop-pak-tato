<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductMasterData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mPrice', function (Blueprint $table) {
            $table->increments('ProductPriceId');
            $table->unsignedInteger('ProductId')->references('ProductId')->on('mProduct');
            $table->decimal('PriceRetail');
            $table->decimal('PriceGrosir');
            $table->decimal('PriceSpesial1');
            $table->decimal('PriceSpesial2');
            $table->decimal('PriceSpesial3');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });

        Schema::create('mProduct', function (Blueprint $table) {
            $table->increments('ProductId');
            $table->unsignedInteger('ProductCategoryId')->references('mProductCategory')->on('ProductCategoryId');
            $table->string('ProductCode',32);
            $table->string('ProductName',50);
            $table->string('ProductDescription',50);
            $table->string('ProductImage',50);//ini kayaknya salah tipe data deh
            $table->string('ProductBarCode',50);
            $table->boolean('IsActive');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });

        Schema::create('mProductCategory', function (Blueprint $table) {
            $table->increments('ProductCategoryId');
            $table->string('ProductCategoryName',50);
            $table->string('ProductCategoryDesc',50);
            $table->string('CategoryType',50);
            $table->boolean('IsActive');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mProductCategory');
        Schema::dropIfExists('mProduct');
        Schema::dropIfExists('mPrice');
    }
}
