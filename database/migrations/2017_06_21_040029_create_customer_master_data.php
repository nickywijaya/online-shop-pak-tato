<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerMasterData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mCustomer', function (Blueprint $table) {
            $table->increments('CustomerId');
            $table->string('CustomerName',50);
            $table->string('CustomerAddress',100);
            $table->string('CustomerPhone1',20);
            $table->string('CustomerPhone2',20);
            $table->string('CustomerContactPerson',50);
            $table->boolean('IsActive');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });

        Schema::create('hCustomerPrice', function (Blueprint $table) {
            $table->increments('CustomerPriceId');
            $table->unsignedInteger('CustomerId')->references('CustomerId')->on('mCustomer');
            $table->decimal('LastPriceRetail');
            $table->decimal('LastPriceGrosir');
            $table->decimal('LastSpecialPrice1');
            $table->decimal('LastSpecialPrice2');
            $table->decimal('LastSpecialPrice3');
            $table->timestamp('CreatedAt')->nullable();
            $table->unsignedInteger('CreatedBy');
            $table->timestamp('ModifiedAt')->nullable();
            $table->unsignedInteger('ModifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mCustomer');
        Schema::dropIfExists('hCustomerPrice');
    }
}
